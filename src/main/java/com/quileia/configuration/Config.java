package com.quileia.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.quileia.entity.EntidadCita;
import com.quileia.entity.EntidadMedico;
import com.quileia.entity.EntidadPaciente;
import com.quileia.model.Cita;
import com.quileia.model.Medico;
import com.quileia.model.Paciente;
import com.quileia.security.jwt.JwtEntryPoint;
import com.quileia.security.jwt.JwtProvider;

@Configuration
@PropertySource("classpath:Component.properties")
public class Config {

	@Bean
	public JwtEntryPoint jwtEntryPoint() {
		return new JwtEntryPoint();
	}

	@Bean
	public JwtProvider jwtProvider() {
		return new JwtProvider();
	}

	@Bean
	public Medico getMedico() {

		return new Medico();
	}

	@Bean
	public Paciente getPaciente() {

		return new Paciente();
	}

	@Bean
	public Cita getCita() {

		return new Cita();
	}

	@Bean
	public EntidadMedico getEntidadMedico() {

		return new EntidadMedico();
	}

	@Bean
	public EntidadPaciente getEntidadPaciente() {

		return new EntidadPaciente();
	}

	@Bean
	public EntidadCita getEntidadCita() {

		return new EntidadCita();
	}

}
