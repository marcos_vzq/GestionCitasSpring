package com.quileia.entity;

import java.sql.Date;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.quileia.security.model.RoleDTO;
import com.quileia.security.model.MainUser;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@SuppressWarnings("serial")
@NoArgsConstructor
@Document(collection = "User")
public class Usuario extends Auditable<String> implements MainUser {

	@Id
	private @Getter @Setter String id;

	private @Getter @Setter String name;

	private @Getter @Setter String lastName;

	private @Getter @Setter Integer identification;

	private @Getter @Setter String username;

	private @Getter @Setter String password;

	private @Getter @Setter Boolean enable;

	private @Getter @Setter Date lastAccess;

	private @Getter @Setter RoleDTO rol = new RoleDTO();

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<GrantedAuthority> build(MainUser mainUser) {
		Usuario user = (Usuario) mainUser;
		RoleDTO rol = user.getRol();
		List<GrantedAuthority> authorities = null;

		authorities = rol.getPermissions().stream()
				.map(permission -> new SimpleGrantedAuthority(permission.getPermissionName()))
				.collect(Collectors.toList());

		return authorities;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return enable;
	}

}
