package com.quileia.entity;

import java.util.Date;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Getter;
import lombok.Setter;

public abstract class Auditable<Entity> {

	@CreatedBy
	protected @Getter @Setter Entity createdBy;

	@CreatedDate

	protected @Getter @Setter Date createdDate;

	@LastModifiedBy
	protected @Getter @Setter Entity lastModifiedBy;

	@LastModifiedDate

	protected @Getter @Setter Date lastModifiedDate;

	private @Getter @Setter String operation;

	public void onPrePersist() {
		this.audit("INSERT");
	}

	public void onPreUpdate() {
		this.audit("UPDATE");
	}

	public void onPreRemove() {
		this.audit("DELETE");
	}

	private void audit(String operation) {
		this.setOperation(operation);
	}

	

	

}
