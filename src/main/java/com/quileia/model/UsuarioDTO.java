package com.quileia.model;

import java.sql.Date;

import com.quileia.security.model.RoleDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class UsuarioDTO {

	private @Getter @Setter String id;
	private @Getter @Setter String name;
	private @Getter @Setter String lastName;
	private @Getter @Setter String identification;
	private @Getter @Setter String username;
	private @Getter @Setter String password;
	private @Getter @Setter Boolean enable;
	private @Getter @Setter Date lastAccess;
	private @Getter @Setter RoleDTO rol;

}
