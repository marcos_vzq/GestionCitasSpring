package com.quileia.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.quileia.entity.EntidadMedico;


@Repository
public interface RepoMedico extends MongoRepository<EntidadMedico, String>, PagingAndSortingRepository<EntidadMedico, String> {

	public Page<EntidadMedico> findAll(Pageable pageable);

}
