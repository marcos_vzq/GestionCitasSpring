package com.quileia.repo;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.quileia.entity.Usuario;

@Repository
public interface UserRepository extends MongoRepository<Usuario, String> {

	Optional<Usuario> findByUsername(String username);

	boolean existsByUsername(String username);

}