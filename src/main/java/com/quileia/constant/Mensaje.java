package com.quileia.constant;

import lombok.Getter;
import lombok.Setter;

public class Mensaje {
	
	@Getter @Setter private String content;
	
	public Mensaje (String content) {
		this.content = content;
	}
}