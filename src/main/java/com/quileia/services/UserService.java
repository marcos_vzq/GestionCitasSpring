package com.quileia.services;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.PropertyAccessorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.quileia.entity.Auditable;
import com.quileia.entity.Usuario;
import com.quileia.repo.UserRepository;

import lombok.Getter;
import lombok.Setter;

@Service
public class UserService implements UserDetailsService {

	private static final Logger logger = LoggerFactory.getLogger(UserService.class);
	private @Getter @Setter Usuario user;

	@Autowired
	private UserRepository userRepository;

	public Optional<Usuario> getByDomain(String username) {
		return this.userRepository.findByUsername(username);
	}

	public boolean existsByDomain(String username) {
		return this.userRepository.existsByUsername(username);
	}

	public Usuario createUser(Usuario user) {
		return this.userRepository.save(user);
	}

	public List<Usuario> getUsers() {
		return this.userRepository.findAll();
	}

	public Usuario getUserById(String id) {
		return this.userRepository.findById(id).get();
	}

	public Usuario getUserByUsername(String username) {
		Optional<Usuario> opUser = this.userRepository.findByUsername(username);
		Usuario user = new Usuario();
		if (opUser.isPresent()) {
			user = opUser.get();
		}
		return user;
	}

	public Usuario updateUser(String id, Usuario newUser) {
		Usuario user = this.getUserById(id);
		// Itera sobre todos los atributos de la clase
		for (Field field : Usuario.class.getDeclaredFields()) {
			try {
				// Si el atributo está en null, le asigna el valor anterior
				if (new PropertyDescriptor(field.getName(), Usuario.class).getReadMethod().invoke(newUser) == null) {
					PropertyAccessorFactory.forDirectFieldAccess(newUser).setPropertyValue(field.getName(),
							new PropertyDescriptor(field.getName(), Usuario.class).getReadMethod().invoke(user));
				}
			} catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException
					| IntrospectionException e) {
				logger.error(e.getMessage());
			}
		}
		// Itera sobre todos los atributos de la clase de auditoria
		for (Field field : Auditable.class.getDeclaredFields()) {
			try {
				// Si el atributo está en null, le asigna el valor anterior
				if (new PropertyDescriptor(field.getName(), Usuario.class).getReadMethod().invoke(newUser) == null) {
					PropertyAccessorFactory.forDirectFieldAccess(newUser).setPropertyValue(field.getName(),
							new PropertyDescriptor(field.getName(), Usuario.class).getReadMethod().invoke(user));
				}
			} catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException
					| IntrospectionException e) {
				logger.error(e.getMessage());
			}
		}
		newUser.setId(id);
		return this.userRepository.save(newUser);
	}

	public void deleteUser(String id) {
		this.userRepository.deleteById(id);
	}

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) {
		Optional<Usuario> opUser = this.userRepository.findByUsername(username);
		this.user = new Usuario();
		if (opUser.isPresent()) {
			this.user = opUser.get();
		}

		User userDetails = new User(user.getUsername(), user.getPassword(), new Usuario().build(this.user));
		return userDetails;

	}

}
